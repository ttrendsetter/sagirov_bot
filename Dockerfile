FROM python:3
WORKDIR /bots
COPY .. /bots
ENV PYTHONPATH "/bots"
RUN pip install --upgrade pip && pip install -r requirements.txt