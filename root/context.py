import os
from dataclasses import dataclass
from typing import Any

import yaml


class Context:
    def __init__(self):
        self.ROOT_PATH = os.path.abspath(os.getcwd())
        self.SCREENS_PATH = os.path.join(self.ROOT_PATH, 'screens')
        default_config_path: str = os.path.join(self.ROOT_PATH, 'default.yaml')
        config_path: str = os.path.join(self.ROOT_PATH, 'config.yaml')
        with open(default_config_path, 'r') as f:
            self.config: dict[str, Any] = yaml.safe_load(f)
        if os.path.exists(config_path):
            with open(config_path, 'r') as f:
                self.config.update(yaml.safe_load(f) or {})
        else:
            with open(config_path, 'w+') as f:
                yaml.dump(self.config, f, default_flow_style=False)

        self.bot_token = self.config['tg_token']
        self.db_conn = self.config['db_conn']
        self.form_url = self.config['form_url']

        screens_path = os.path.join(self.ROOT_PATH, 'screens')
        if not os.path.exists(screens_path):
            os.makedirs(screens_path)


@dataclass
class UserData:
    first_name = ''
    last_name = ''
    email = ''
    phone = ''
    birthday = ''
    user_id = None

    def structure_data(self) -> dict[str, str]:
        return {
            'first_name': self.first_name,
            'last_name': self.last_name,
            'email': self.email,
            'phone': self.phone,
            'birthday': self.birthday,
            'user_id': self.user_id
        }


context = Context()
