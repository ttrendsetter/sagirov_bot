import os
from datetime import datetime
from time import sleep

import requests
from selenium.common import TimeoutException

from root.context import context
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from root.db_config import crt_session
from root.models import User
from root.telegram_bot_actions import send_photo, send_error
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class RegisterBot:
    def __init__(self, **kwargs):
        self.driver = webdriver.Remote("http://selenium:4444/wd/hub", DesiredCapabilities.CHROME)

        self.firstname = kwargs.get('first_name')
        self.lastname = kwargs.get('last_name')
        self.email = kwargs.get('email')
        self.phone = kwargs.get('phone')
        self.birthday = kwargs.get('birthday')
        self.user_id = kwargs.get('user_id')

    def get_by_xpath(self, xpath: str) -> 'WebElement':
        return self.driver.find_element(By.XPATH, xpath)

    def wait_for_element(self, element):
        element_present = EC.presence_of_element_located((By.XPATH, element))
        WebDriverWait(self.driver, 10).until(element_present)

    def fill_first_page(self):
        self.wait_for_element('//form//input[@name="name"]')
        self.get_by_xpath('//form//input[@name="name"]').send_keys(self.firstname)

        self.get_by_xpath('//form//input[@name="lastname"]').send_keys(self.lastname)

        self.get_by_xpath('//form//button').click()

    def fill_second_page(self):
        self.wait_for_element('//form//input[@name="email"]')
        self.get_by_xpath('//form//input[@name="email"]').send_keys(self.email)
        self.get_by_xpath('//form//input[@name="phone"]').send_keys(self.phone)
        self.get_by_xpath('(//form//button)[2]').click()

    def fill_third_page(self):
        self.wait_for_element('//form//input[@type="string"]')
        self.get_by_xpath('//form//input[@type="string"]').click()

        self.get_by_xpath(f'(//div[@class="vdpPeriodControl"])[2]/select/option[@value={self.birthday.year}]').click()
        self.get_by_xpath(
            f'(//div[@class="vdpPeriodControl"])[1]/select/option[@value={self.birthday.month - 1}]').click()
        self.get_by_xpath(f'//td[@data-id="{self.birthday.year}-{self.birthday.month}-{self.birthday.day}"]').click()
        self.get_by_xpath('//form//button[@type="submit"]').click()

    def take_screenshot(self, filename):
        """
        Takes a successful page screen
        """
        self.wait_for_element('//div[@class="b24-form-loader"and@style="display: none;"]')
        self.driver.get_screenshot_as_file(os.path.join(context.SCREENS_PATH, filename))

    def update_user(self, filename):
        """
        Updates a user row in the User table
        """
        # Updating database and set user registered to True
        session = crt_session()
        session.query(User).filter(User.user_id == self.user_id).update({
            'filename': filename,
            'registered': True,
        })
        session.commit()
        session.close()

    def remove_user(self):
        session = crt_session()
        session.query(User).filter(User.user_id == self.user_id).delete()
        session.commit()
        session.close()

    def run(self):
        """
        Fills registration form and sends a successful screen to the user
        """
        self.driver.get(context.form_url)

        # Fill the registration pages
        try:
            self.fill_first_page()
            self.fill_second_page()
            self.fill_third_page()
            # Takes a screenshot and send him to the user
            filename = f'{datetime.strftime(datetime.utcnow(), "%Y-%m-%d_%H.%m")}_{self.user_id}.jpg'
            self.take_screenshot(filename)
            self.update_user(filename)
            send_photo(self.user_id, filename)
        except TimeoutException:
            send_error(self.user_id)
            self.remove_user()

        self.driver.close()


def run_register_bot():
    """
    Checks the form is available, gets users and sends them data to the page
    """
    start = datetime.utcnow()
    while True:
        # Every 10 minutes checks that form is available
        if (datetime.utcnow() - start).total_seconds() // 60 == 10:
            if requests.get(context.form_url).status_code != 200:
                # waiting if form is not available
                sleep(600)
            start = datetime.utcnow()
        else:
            # Choosing first 10 user what are not registered and try to register them
            session = crt_session()
            users = session.query(User).filter(User.registered == 'false').limit(10).all()
            session.close()
            for user in users:
                registration_bot = RegisterBot(**user.structure_data())
                registration_bot.run()
                sleep(1)
