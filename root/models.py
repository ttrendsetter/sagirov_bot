from sqlalchemy import Integer, Column, String, Date, Boolean
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    user_id = Column(String)
    first_name = Column(String)
    last_name = Column(String)
    phone = Column(String)
    email = Column(String)
    birthday = Column(Date)
    filename = Column(String)

    registered = Column(Boolean, default=False, server_default='false')

    def structure_data(self):
        return {
            'first_name': self.first_name,
            'last_name': self.last_name,
            'email': self.email,
            'phone': self.phone,
            'birthday': self.birthday,
            'user_id': self.user_id
        }
