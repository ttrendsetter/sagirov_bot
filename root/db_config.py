from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session

from root.context import context


def crt_engine() -> Engine:
    engine = create_engine(context.db_conn)
    return engine


def crt_session() -> Session:
    return Session(crt_engine())
