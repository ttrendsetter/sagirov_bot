import os

from root.telegram_bot import bot
from datetime import datetime
import re
from root.db_config import crt_session
from root.models import User
from root.context import UserData, context


@bot.message_handler(content_types=['text'])
def start(message):
    """
    Starting messaging with bot,the next step is to ask for a name
    """
    if message.text.strip() == '/reg':
        bot.send_message(message.from_user.id, 'Hello')
        bot.send_message(message.from_user.id, 'What is your name?')
        bot.register_next_step_handler(message, get_name, user=UserData())

    else:
        bot.send_message(message.from_user.id, 'Send me /reg')


def get_name(message, user: UserData):
    """
    Getting a name -> ask for surname
    """
    user.first_name = message.text.strip().capitalize()
    bot.send_message(message.from_user.id, 'What is your surname?')
    bot.register_next_step_handler(message, get_last_name, user=user)


def get_last_name(message, user: UserData):
    """
    Getting surname -> ask for email
    """
    user.last_name = message.text.strip().capitalize()
    bot.send_message(message.from_user.id, f'Send me your email')
    bot.register_next_step_handler(message, get_email, user=user)


def get_email(message, user: UserData):
    """
    Getting email -> ask for phone
    """
    r = r'^[a-zA-Z][a-zA-Z0-9\.\_\-]*\@[a-zA-Z]+\.[a-z]{1,3}$'
    email = message.text.strip()
    if re.match(r, email):
        user.email = email
        bot.send_message(message.from_user.id, f'Now send your phone')
        bot.register_next_step_handler(message, get_phone, user=user)
    else:
        bot.send_message(message.from_user.id, 'Please, send a valid email')
        bot.register_next_step_handler(message, get_email, user=user)


def get_phone(message, user: UserData):
    """
    Getting phone -> ask about the birthday
    """
    r = r'^((8|\+7)[\- ]?)(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$'
    phone = message.text.strip()
    if re.match(r, phone):
        user.phone = phone
        bot.send_message(message.from_user.id, 'When is your birthday (dd.mm.yyyy)?')
        bot.register_next_step_handler(message, get_birthday, user=user)
    else:
        bot.send_message(message.from_user.id, 'Pls, send valid phone')
        bot.register_next_step_handler(message, get_phone, user=user)


def get_birthday(message, user: UserData):
    """
    Getting birthday -> Confirmation of input data
    """
    try:
        birthday = datetime.strptime(message.text.strip(), '%d.%m.%Y')
        if birthday.year < 1902 and birthday < datetime.utcnow():
            raise ValueError
        user.birthday = birthday
        bot.send_message(message.from_user.id, f'You are {user.first_name} {user.last_name}'
                                               f'\nYour contact info {user.phone}, {user.email}'
                                               f'\nYour birthday {user.birthday.date()}')

        bot.send_message(message.from_user.id, 'Is everything right? y/n')
        bot.register_next_step_handler(message, confirm, user=user)

    except ValueError:
        bot.send_message(message.from_user.id, 'Please, use format dd.mm.yyyy')
        bot.register_next_step_handler(message, get_birthday, user=user)


def confirm(message, user: UserData):
    """
    Confirmation data -> registration again | writing data to the database
    """
    if message.text.lower() == 'y':
        user.user_id = message.from_user.id
        register(user=user)
    elif message.text.lower() == 'n':
        start(message)


def register(user: UserData):
    """
    Creating a new note about user
    """
    new_user = User(
        user_id=user.user_id,
        first_name=user.first_name,
        last_name=user.last_name,
        phone=user.phone,
        email=user.email,
        birthday=user.birthday
    )
    session = crt_session()
    session.add(new_user)
    session.commit()
    session.close()


def send_photo(user_id: str, filename: str):
    """
    Sending photo to the user
    :param user_id: User's tg id
    """
    with open(os.path.join(context.SCREENS_PATH, filename), 'rb') as f:
        bot.send_photo(user_id, f)


def send_error(user_id: str):
    """
    Sending message error to the user
    :param user_id: User's tg id
    """
    bot.send_message(user_id, 'Sorry, something went wrong\nPlease try again')


def run_telegram_bot():
    bot.polling(none_stop=True, interval=5)
