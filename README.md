# SAGIROV BOT
## Preprocessing
- Set your configs to the config.yaml file like in default.yaml
```YAML
tg_token: 'YOUR_TOKEN'
db_conn: 'YOUR_DB_CONNECTION_STRING'
form_url: 'BACKEND_API_URL'
```
- Change db environments in docker-compose file if you need

## Start
run command "docker-compose up"
