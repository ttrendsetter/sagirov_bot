from root.models import Base
from root.db_config import crt_engine

if __name__ == '__main__':
    # create tables in database
    Base.metadata.drop_all(crt_engine())
    Base.metadata.create_all(crt_engine())
